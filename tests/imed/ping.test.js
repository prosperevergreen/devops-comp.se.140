const axios = require('axios').default;

const imedURL = process.env.IMED_URL || 'http://0.0.0.0:2222';

describe('/ping', () => {
  test('should ping IMED to be alive and connected', async () => {
    const res = await axios.get(`${imedURL}/ping`);
    expect(res.status).toBe(200);
    expect(res.statusText).toBe('OK');
    expect(res.data).toBe('Pong!!!');
  });
});
