const axios = require('axios').default;

// const apiURL = process.env.API_URL || 'http://0.0.0.0:8083';
const origURL = process.env.ORIG_URL || 'http://0.0.0.0:1111';

const delay = (seconds, cb = null) =>
  new Promise((resolve) => {
    setTimeout(() => {
      return typeof cb === 'function' ? resolve(cb()) : resolve(cb);
    }, seconds * 1000);
  });

// possible states
const states = {
  init: 'INIT',
  running: 'RUNNING',
  paused: 'PAUSED',
  shutdown: 'SHUTDOWN',
};

describe('/state', () => {
  afterEach(async () => {
    await axios.put(`${origURL}/state`, { payload: 'PAUSED' });
  });

  test('should set state successfully', async () => {
    const put = await axios.put(`${origURL}/state`, {
      payload: states.paused,
    });
    expect(put.status).toBe(200);
    expect(put.statusText).toBe('OK');
  });

  test('should GET the state of ORIG as RUNNING after INIT', async () => {
    await axios.put(`${origURL}/state`, { payload: states.init });
    await delay(2);
    const res = await axios.get(`${origURL}/state`);
    expect(res.status).toBe(200);
    expect(res.statusText).toBe('OK');
    expect(res.data.payload).toBe(states.running);
  });

  test('should SET (PUT) the state of the ORIG to RUNNING', async () => {
    await axios.put(`${origURL}/state`, { payload: states.running });
    await delay(2);
    const get = await axios.get(`${origURL}/state`);
    expect(get.status).toBe(200);
    expect(get.statusText).toBe('OK');
    expect(get.data.payload).toBe(states.running);
  });

  test('should SET (PUT) the state of the ORIG to PAUSED', async () => {
    await axios.put(`${origURL}/state`, { payload: states.paused });
    const get = await axios.get(`${origURL}/state`);
    expect(get.status).toBe(200);
    expect(get.statusText).toBe('OK');
    expect(get.data.payload).toBe(states.paused);
  });
});

describe('/run-log', () => {
  test('should get log of commands sent to ORIG', async () => {
    await axios.put(`${origURL}/state`, { payload: states.init });
    await delay(0.5);
    await axios.put(`${origURL}/state`, { payload: states.paused });
    await axios.put(`${origURL}/state`, { payload: states.running });
    await delay(0.5);
    await axios.put(`${origURL}/state`, { payload: states.paused });
    const res = await axios.get(`${origURL}/run-log`);
    expect(res.status).toBe(200);
    expect(res.statusText).toBe('OK');
    const payload = res.data.payload.join(',');
    expect(payload).toContain(states.init);
    expect(payload).toContain(states.paused);
    expect(payload).toContain(states.running);
  });
});
