const axios = require('axios').default;

const obseURL = process.env.OBSE_URL || 'http://0.0.0.0:3333';

describe('/ping', () => {
  test('should ping OBSE to be alive and connected', async () => {
    const res = await axios.get(`${obseURL}/ping`);
    expect(res.status).toBe(200);
    expect(res.statusText).toBe('OK');
    expect(res.data).toBe('Pong!!!');
  });
});
