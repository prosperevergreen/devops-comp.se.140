const axios = require('axios').default;

const rabbitmqServerURL =
  process.env.RABBITMQ_SERVER_URL || 'http://0.0.0.0:3000';

describe('/state', () => {
  test('RabbitMQ should be running', async () => {
    const res = await axios.get(`${rabbitmqServerURL}/state`);
    expect(res.status).toBe(200);
    expect(res.statusText).toBe('OK');
    expect(res.data.running).toBe(true);
    expect(res.data.uptime).not.toBe(0);
  });
});
