const axios = require('axios').default;

const apiURL = process.env.API_URL || 'http://0.0.0.0:8083';

const delay = (seconds, cb = null) =>
  new Promise((resolve) => {
    setTimeout(() => {
      return typeof cb === 'function' ? resolve(cb()) : resolve(cb);
    }, seconds * 1000);
  });

// possible states
const states = {
  init: 'INIT',
  running: 'RUNNING',
  paused: 'PAUSED',
  shutdown: 'SHUTDOWN',
};

describe('/run-log', () => {
  test('should get log of commands sent to ORIG', async () => {
    await axios.put(`${apiURL}/state`, { payload: states.init });
    await delay(0.5);
    await axios.put(`${apiURL}/state`, { payload: states.paused });
    await axios.put(`${apiURL}/state`, { payload: states.running });
    await delay(0.5);
    await axios.put(`${apiURL}/state`, { payload: states.paused });
    const res = await axios.get(`${apiURL}/run-log`);
    expect(res.status).toBe(200);
    expect(res.statusText).toBe('OK');
    expect(res.data).toContain(states.init);
    expect(res.data).toContain(states.paused);
    expect(res.data).toContain(states.running);
  });
});
