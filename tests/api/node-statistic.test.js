const axios = require('axios').default;

const apiURL = process.env.API_URL || 'http://0.0.0.0:8083';

describe('/node-statistic', () => {
  test('should return core statistics (five (5)) of the RabbitMQ. ', async () => {
    const res = await axios.get(`${apiURL}/node-statistic`);
    expect(res.status).toBe(200);
    expect(res.statusText).toBe('OK');
    expect(res.data.fd_used).toBeDefined();
    expect(res.data.mem_used).toBeDefined();
    expect(res.data.running).toBeTruthy();
    expect(res.data.sockets_used).toBeDefined();
    expect(res.data.processors).toBeDefined();
  });
});
