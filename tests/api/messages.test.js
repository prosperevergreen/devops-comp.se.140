const axios = require('axios').default;

const apiURL = process.env.API_URL || 'http://0.0.0.0:8083';

const delay = (seconds, cb = null) =>
  new Promise((resolve) => {
    setTimeout(() => {
      return typeof cb === 'function' ? resolve(cb()) : resolve(cb);
    }, seconds * 1000);
  });

// possible states
const states = {
  init: 'INIT',
  running: 'RUNNING',
  paused: 'PAUSED',
  shutdown: 'SHUTDOWN',
};

describe('/messages', () => {
  test('should not get empty message after messages have be published', async () => {
    await axios.put(`${apiURL}/state`, { payload: states.init });
    await delay(2);
    await axios.put(`${apiURL}/state`, { payload: states.paused });
    const res = await axios.get(`${apiURL}/messages`);
    expect(res.status).toBe(200);
    expect(res.statusText).toBe('OK');
    expect(res.data).not.toBe('');
    expect(res.data).toContain('Topic my.o: MSG_1');
    expect(res.data).toContain('Topic my.i: Got MSG_1');
  });
});
