const axios = require('axios').default;

const apiURL = process.env.API_URL || 'http://0.0.0.0:8083';

describe('/queue-statistic', () => {
  test('should return queue statistics of the nodes in the RabbitMQ. ', async () => {
    const res = await axios.get(`${apiURL}/queue-statistic`);
    expect(res.status).toBe(200);
    expect(res.statusText).toBe('OK');
    expect(res.data).toBeInstanceOf(Array);
    res.data.forEach((queue) => {
      expect(queue).toHaveProperty('name');
      expect(queue).toHaveProperty('node');
      expect(queue).toHaveProperty('consumers');
      expect(queue.state).toBe('running');
    });
  });
});
