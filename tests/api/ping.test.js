const axios = require('axios').default;

const apiURL = process.env.API_URL || 'http://0.0.0.0:8083';

describe('/ping', () => {
  test('should ping the api gateway', async () => {
    const res = await axios.get(`${apiURL}/ping`);
    expect(res.status).toBe(200);
    expect(res.statusText).toBe('OK');
    expect(res.data).toBe('pinged api!!!');
  });
});
