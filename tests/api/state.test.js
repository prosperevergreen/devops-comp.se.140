const axios = require('axios').default;

const apiURL = process.env.API_URL || 'http://0.0.0.0:8083';

const delay = (seconds, cb = null) =>
  new Promise((resolve) => {
    setTimeout(() => {
      return typeof cb === 'function' ? resolve(cb()) : resolve(cb);
    }, seconds * 1000);
  });

// possible states
const states = {
  init: 'INIT',
  running: 'RUNNING',
  paused: 'PAUSED',
  shutdown: 'SHUTDOWN',
};

describe('/state', () => {
  afterEach(async () => {
    await axios.put(`${apiURL}/state`, { payload: 'PAUSED' });
  });

  test('should set state successfully', async () => {
    const put = await axios.put(`${apiURL}/state`, {
      payload: states.paused,
    });
    expect(put.status).toBe(200);
    expect(put.statusText).toBe('OK');
  });

  test('should GET the state of ORIG as RUNNING after INIT', async () => {
    await axios.put(`${apiURL}/state`, { payload: states.init });
    await delay(2);
    const res = await axios.get(`${apiURL}/state`);
    expect(res.status).toBe(200);
    expect(res.statusText).toBe('OK');
    expect(res.data).toBe(states.running);
  });

  test('should SET (PUT) the state of the ORIG to RUNNING', async () => {
    await axios.put(`${apiURL}/state`, { payload: states.running });
    await delay(2);
    const get = await axios.get(`${apiURL}/state`);
    expect(get.status).toBe(200);
    expect(get.statusText).toBe('OK');
    expect(get.data).toBe(states.running);
  });

  test('should SET (PUT) the state of the ORIG to PAUSED', async () => {
    await axios.put(`${apiURL}/state`, { payload: states.paused });
    const get = await axios.get(`${apiURL}/state`);
    expect(get.status).toBe(200);
    expect(get.statusText).toBe('OK');
    expect(get.data).toBe(states.paused);
  });
});
