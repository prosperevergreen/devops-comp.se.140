const http = require('http');
const axios = require('axios').default;
const express = require('express');
const amqp = require('amqplib/callback_api');
const { createLogger, format, transports } = require('winston');

// Set server port
const PORT = 8083;
const origURL = process.env.ORIG_URL || 'http://0.0.0.0:1111';
const httpservURL = process.env.HTTPSERV_URL || 'http://0.0.0.0:8080';
const rabbitmqURL = process.env.RABBITMQ_URL || 'http://0.0.0.0:15672';
const rabbitmqServerURL =
  process.env.RABBITMQ_SERVER_URL || 'http://0.0.0.0:3000';
const amqpUrl = process.env.AMQP_URL || 'amqp://0.0.0.0:5672';
const logsUrl = process.env.LOGS_URL || 'http://0.0.0.0:1122';

// Setup logger config
const { printf } = format;
const myFormat = printf(({ level, message, label, timestamp }) => {
  return `[${label}] ${level.toUpperCase()} ${timestamp}: ${message}`;
});
const { combine, timestamp, label } = format;
const logFile = '/logs/debug.log';
const logger = createLogger({
  level: 'debug',
  format: combine(label({ label: 'API' }), timestamp(), myFormat),
  transports: [
    new transports.Console(),
    new transports.File({ filename: logFile }),
  ],
});

// SETUP SERVER
const app = express();
// configure server app
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
// states
const states = {
  init: 'INIT',
  running: 'RUNNING',
  paused: 'PAUSED',
  shutdown: 'SHUTDOWN',
};
let sendShutDownMsg = null;

// A function to delay/sleep for a certain time in seconds
// also option for running a callback function after the delay
const delay = (seconds, cb = null) =>
  new Promise((resolve) => {
    setTimeout(() => {
      return typeof cb === 'function' ? resolve(cb()) : resolve(cb);
    }, seconds * 1000);
  });

// Handle GET /state request by getting the current state
app.get('/ping', async (req, res) => {
  logger.info(`${req.method} ${req.path} from ${req.ip} initiated`);
  res.status(200).send('pinged api!!!');
  logger.info(`${req.method} ${req.path} from ${req.ip} completed`);
});

// Handle GET /messages request by setting the state
app.get('/messages', async (req, res, next) => {
  try {
    logger.info(`${req.method} ${req.path} from ${req.ip} initiated`);
    const getRes = await axios.get(httpservURL);
    res.status(getRes.status).send(getRes.data);
    logger.info(`${req.method} ${req.path} from ${req.ip} completed`);
  } catch (err) {
    logger.error(
      `GET /messages from ${req.ip} caused error because ${JSON.stringify(err)}`
    );
    next(err);
  }
});

// Handle PUT /state request by setting the state
app.put('/state', async (req, res, next) => {
  try {
    logger.info(
      `${req.method} ${req.path} from ${req.ip} with payload ${JSON.stringify(
        req.body
      )} initiated`
    );
    if (req.body.payload === states.shutdown) {
      if (!sendShutDownMsg) throw new Error('Cannot send shutdown messgae');
      res.status(200).send('Shutting down all services');
      await axios.put(`${httpservURL}/state`, req.body);
      logger.info('Shut down HTTPSERV service');
      await axios.put(`${logsUrl}/state`, req.body);
      logger.info('Shut down LOGS service');
      logger.info(
        `${req.method} ${req.path} from ${req.ip} with payload ${JSON.stringify(
          req.body
        )} completed`
      );
      sendShutDownMsg(req.body.payload);
      await axios.put(`${rabbitmqServerURL}/state`, req.body);
      logger.info('Shut down RABBITMQ service');
      logger.warn('Shutting down!!!');
      process.exit(0);
      return;
    }
    const putRes = await axios.put(`${origURL}/state`, req.body);
    logger.info(
      `${req.method} ${req.path} from ${req.ip} with payload ${JSON.stringify(
        req.body
      )} completed`
    );
    res.status(putRes.status).send(putRes.data.payload);
  } catch (err) {
    logger.error(
      `${req.method} ${req.path} from ${req.ip} with payload ${
        req.body
      } caused error because ${JSON.stringify(err)}`
    );
    next(err);
  }
});

// Handle GET /state request by getting the current state
app.get('/state', async (req, res, next) => {
  try {
    logger.info(`${req.method} ${req.path} from ${req.ip} initiated`);
    const getRes = await axios.get(`${origURL}/state`);
    logger.info(`${req.method} ${req.path} from ${req.ip} completed`);
    res.status(getRes.status).send(getRes.data.payload);
  } catch (err) {
    logger.error(
      `GET /state from ${req.ip} caused error because ${JSON.stringify(err)}`
    );
    next(err);
  }
});

// Get thw command given to ORIG service
app.get('/run-log', async (req, res, next) => {
  try {
    logger.info(`${req.method} ${req.path} from ${req.ip} initiated`);
    const getRes = await axios.get(`${origURL}/run-log`);
    logger.info(`${req.method} ${req.path} from ${req.ip} completed`);
    res.status(getRes.status).send(getRes.data.payload.join('\n'));
  } catch (err) {
    logger.error(
      `GET /run-log from ${req.ip} caused error because ${JSON.stringify(err)}`
    );
    next(err);
  }
});

// Handle GET /node-statistic request by getting the statistics of rabbit mq
app.get('/node-statistic', async (req, res, next) => {
  logger.info(`${req.method} ${req.path} from ${req.ip} initiated`);
  try {
    const getStatsRes = await axios.get(`${rabbitmqURL}/api/nodes`, {
      auth: {
        username: 'guest',
        password: 'guest',
      },
    });
    // Extracts the 5 core statistics
    // eslint-disable-next-line camelcase
    const [{ fd_used, mem_used, running, sockets_used, processors }] =
      getStatsRes.data;
    logger.info(`${req.method} ${req.path} from ${req.ip} completed`);
    res
      .status(200)
      // eslint-disable-next-line camelcase
      .json({ fd_used, mem_used, running, sockets_used, processors });
  } catch (err) {
    logger.error(
      `${req.method} ${req.path} from ${req.ip} 
      caused error because ${JSON.stringify(err)}`
    );
    next(err);
  }
});

// Handle GET /node-statistic request by getting the statistics of rabbit mq
app.get('/queue-statistic', async (req, res, next) => {
  try {
    logger.info(`${req.method} ${req.path} from ${req.ip} initiated`);
    const getStatsRes = await axios.get(`${rabbitmqURL}/api/queues`, {
      auth: {
        username: 'guest',
        password: 'guest',
      },
    });
    res.status(200).json(getStatsRes.data);
    logger.info(`${req.method} ${req.path} from ${req.ip} completed`);
  } catch (err) {
    logger.error(
      `${req.method} ${req.path} from ${
        req.ip
      } caused error because ${JSON.stringify(err)}`
    );
    next(err);
  }
});

// Respond with error for all other requests
app.all('*', (req, res) => {
  logger.warn(`${req.method} ${req.path} from ${req.ip} Not allowed`);
  res.status(400).send('Not allowed');
});

// Creates server
const server = http.createServer(app);

// Server error listener
server.on('error', (err) => {
  logger.error(`Server crashed: ${JSON.stringify(err)}`);
  server.close();
});

// Close server
server.on('close', () => logger.warn('Server closed.'));

// SETUP ORIG MESSAGING
// Connect to rabbitmq
amqp.connect(amqpUrl, (error0, connection) => {
  if (error0) {
    logger.error(`connecting to rabbitmq: ${JSON.stringify(error0)}`);
    server.close();
    throw error0;
  }

  // Server starts listening when connection to rabbitmq is succesfull
  server.listen(PORT, () => {
    logger.info(`Started listening on ports: ${PORT}`);
  });

  // Create a channel for transactions in the rabbitmq
  connection.createChannel((error1, channel) => {
    if (error1) {
      logger.error(`connecting to rabbitmq channel: ${JSON.stringify(error1)}`);
      throw error1;
    }
    logger.info('connected to rabbitmq');
    const exchange = 'devops_amqp_ex';
    const pubTopic = 'api';
    // check that the exchange channel exists
    channel.assertExchange(exchange, 'topic', {
      durable: false,
    });
    logger.info(`connected to exchange: '${exchange}'`);

    // assign sendShutDownMsg variable to a function for sending massages
    sendShutDownMsg = (msg) => {
      channel.publish(exchange, pubTopic, Buffer.from(msg));
      logger.info(`published: ${msg}, topic: ${pubTopic}`);
      delay(4, () => {
        connection.close();
      });
    };
  });
});
