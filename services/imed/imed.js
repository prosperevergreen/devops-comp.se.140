const amqp = require('amqplib/callback_api');
const { createLogger, format, transports } = require('winston');
const http = require('http');

const amqpUrl = process.env.AMQP_URL || 'amqp://0.0.0.0:5672';
const states = {
  init: 'INIT',
  running: 'RUNNING',
  paused: 'PAUSED',
  shutdown: 'SHUTDOWN',
};
const PORT = 2222;

// Setup logger config
const { printf } = format;
const myFormat = printf(({ level, message, label, timestamp }) => {
  return `[${label}] ${level.toUpperCase()} ${timestamp}: ${message}`;
});
const { combine, timestamp, label } = format;
const logFile = '/logs/debug.log';
const logger = createLogger({
  level: 'debug',
  format: combine(label({ label: 'IMED' }), timestamp(), myFormat),
  transports: [
    new transports.Console(),
    new transports.File({ filename: logFile }),
  ],
});

// Create server
const server = http.createServer((req, res) => {
  res.setHeader('Content-Type', 'text/html');
  if (req.url === '/ping' && req.method === 'GET') {
    res.statusCode = 200;
    return res.end('Pong!!!');
  }
  res.statusCode = 400;
  return res.end('Not allowed!!!');
});

// Server error listener
server.on('error', (err) => {
  logger.error(`Server crashed: ${JSON.stringify(err)}`);
  server.close();
});

// Close server
server.on('close', () => logger.warn('Server closed.'));

// Connect to rabbitmq
amqp.connect(amqpUrl, (error0, connection) => {
  if (error0) {
    logger.error(`connecting to rabbitmq: ${JSON.stringify(error0)}`);
    server.close();
    throw error0;
  }
  logger.info('connected to rabbitmq');

  // Server starts listening when connection to rabbitmq is succesfull
  server.listen(PORT, () => {
    logger.info(`Started listening on ports: ${PORT}`);
  });

  // create channel for transactions
  connection.createChannel((error1, channel) => {
    if (error1) {
      logger.error(`connecting to rabbitmq channel: ${JSON.stringify(error1)}`);
      throw error1;
    }
    logger.info('created to channel for exchange rabbitmq');

    const exchange = 'devops_amqp_ex';
    const pubTopic = 'my.i';
    const subTopics = ['my.o', 'api'];
    // check that exchange channel exist
    // configure exchange channel
    channel.assertExchange(exchange, 'topic', {
      durable: false,
    });
    logger.info(`connected to exchange: '${exchange}'`);
    // Check that queue exists
    channel.assertQueue(
      '',
      {
        exclusive: true,
      },
      (error2, q) => {
        if (error2) {
          logger.error('queue assertion');
          throw error2;
        }
        logger.info('successfully connect to queue');
        // Subscribe to the queues
        subTopics.forEach((subTopic) => {
          channel.bindQueue(q.queue, exchange, subTopic);
          logger.info(`subscribed to topic '${subTopic}'`);
        });

        // Listens for message from the queue
        channel.consume(q.queue, (msg) => {
          switch (msg.content.toString()) {
            // Check if message is shutdown message
            case states.shutdown: {
              connection.close();
              logger.warn('Shutting down!!!');
              process.exit(0);
              break;
            }
            default: {
              const newMsg = `Got ${msg.content.toString()}`;
              // Publish recieved message to pubTopic
              channel.publish(exchange, pubTopic, Buffer.from(newMsg));
              logger.info(
                `Recieved: ${msg.content.toString()}, topic: ${
                  msg.fields.routingKey
                }`
              );
              logger.info(`published: ${newMsg}, topic: ${pubTopic}`);
              break;
            }
          }
        });
      }
    );
  });
});
