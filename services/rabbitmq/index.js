const express = require('express');
const http = require('http');
const axios = require('axios').default;
// states
const states = {
  init: 'INIT',
  running: 'RUNNING',
  paused: 'PAUSED',
  shutdown: 'SHUTDOWN',
};
const rabbitmqURL = process.env.RABBITMQ_URL || 'http://0.0.0.0:15672';
const PORT = 3000;
const app = express();

// configure server app
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// Handle PUT /state request
app.put('/state', (req, res) => {
  if (req.body.payload === states.shutdown) {
    res.status(200).send('Shutdown');
    process.exit(0);
  }
  res.status(200).send("I don't know what you want");
});

// Handle GET /state of rabbit mq
app.get('/state', async (req, res, next) => {
  try {
    const getStatsRes = await axios.get(`${rabbitmqURL}/api/nodes`, {
      auth: {
        username: 'guest',
        password: 'guest',
      },
    });
    // Extracts core statistics
    const [{ uptime, running }] = getStatsRes.data;
    res.status(200).json({ uptime, running });
  } catch (err) {
    next(err);
  }
});

// Creates server
const server = http.createServer(app);

// Server error listener
server.on('error', (err) => {
  console.error(`Server crashed: ${err}`);
  server.close();
});

// Close server
server.on('close', () => console.warn('Server closed.'));

// Server starts listening when connection to rabbitmq is succesfull
server.listen(PORT, () => {
  console.log(`Listening on ports: ${PORT}`);
});
