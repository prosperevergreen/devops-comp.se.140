const http = require('http');
const fs = require('fs');
const express = require('express');
const { createLogger, format, transports } = require('winston');

// Set server port
const PORT = 8080;

const outputFile = '/output/output.txt';
// states
const states = {
  init: 'INIT',
  running: 'RUNNING',
  paused: 'PAUSED',
  shutdown: 'SHUTDOWN',
};

// Setup logger config
const { printf } = format;
const myFormat = printf(({ level, message, label, timestamp }) => {
  return `[${label}] ${level.toUpperCase()} ${timestamp}: ${message}`;
});
const { combine, timestamp, label } = format;
const logFile = '/logs/debug.log';
const logger = createLogger({
  level: 'debug',
  format: combine(label({ label: 'API' }), timestamp(), myFormat),
  transports: [
    new transports.Console(),
    new transports.File({ filename: logFile }),
  ],
});

// SETUP SERVER
const app = express();
// configure server app
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// Handle PUT /state request
app.put('/state', (req, res) => {
  logger.info(`${req.method} ${req.path} from ${req.ip} initiated`);
  if (req.body.payload === states.shutdown) {
    res.status(200).send();
    logger.warn('Shutting down!!!');
    process.exit(0);
  }
  res.status(200).send("I don't know what you want");
  logger.info(`${req.method} ${req.path} from ${req.ip} completed`);
});

// GET messages saved by OBSE
app.get('/', (req, res) => {
  try {
    logger.info(`${req.method} ${req.path} from ${req.ip} initiated`);
    const data = fs.readFileSync(outputFile, { encoding: 'utf8', flag: 'r' });
    res.status(200).send(data);
    logger.info(`${req.method} ${req.path} from ${req.ip} completed`);
  } catch (err) {
    res.status(500).send(err);
    logger.error(
      `${req.method} ${req.path} from ${
        req.ip
      } crashed server because ${JSON.stringify(err)}`
    );
  }
});

app.all('*', (req, res) => {
  logger.warn(`${req.method} ${req.path} from ${req.ip} Not allowed`);
  res.status(400).send('Not allowed');
});
// Creates server
const server = http.createServer(app);

// Server error listener async
server.on('error', (err) => {
  logger.error(`Server crashed: ${JSON.stringify(err)}`);
  server.close();
});

// Close server
server.on('close', () => logger.warn('Server closed.'));

// Server starts listening
server.listen(PORT, () => {
  logger.info(`Started listening on ports: ${PORT}`);
});
