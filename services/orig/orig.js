const amqp = require('amqplib/callback_api');
const express = require('express');
const http = require('http');
const { createLogger, format, transports } = require('winston');

// Set server port
const PORT = 1111;
// Set rabbitMQ url
const amqpUrl = process.env.AMQP_URL || 'amqp://0.0.0.0:5672';
// possible states
const states = {
  init: 'INIT',
  running: 'RUNNING',
  paused: 'PAUSED',
  shutdown: 'SHUTDOWN',
};

let stateLogs = [];
let currentState = null;
let interval = null;
let setState = null;
let messageCounter = 1;

// Setup logger config
const { printf } = format;
const myFormat = printf(({ level, message, label, timestamp }) => {
  return `[${label}] ${level.toUpperCase()} ${timestamp}: ${message}`;
});
const { combine, timestamp, label } = format;
const logFile = '/logs/debug.log';
const logger = createLogger({
  level: 'debug',
  format: combine(label({ label: 'ORIG' }), timestamp(), myFormat),
  transports: [
    new transports.Console(),
    new transports.File({ filename: logFile }),
  ],
});

// SETUP SERVER
const app = express();
// configure server app
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// Handle GET /state request by getting the current state
app.get('/state', (req, res) => {
  logger.info(`GET /ping from ${req.ip} initiated`);
  res.status(200).json({ payload: currentState });
  logger.info(`GET /ping from ${req.ip} completed`);
});

// Handle PUT /state request by setting the state
app.put('/state', (req, res) => {
  logger.info(`${req.method} ${req.path} from ${req.ip} initiated`);
  if (!setState) {
    logger.error(`PUT /state from ${req.ip} without state data`);
    res.status(500).send('Cannot set state');
  } else if (req.body.payload === states.shutdown) {
    res.status(200).send({ payload: states.shutdown });
    logger.info(`GET /messages from ${req.ip} completed`);
    setState(req.body.payload);
  } else {
    setState(req.body.payload);
    res.status(200).json({ payload: currentState });
    logger.info(`GET /messages from ${req.ip} completed`);
  }
});

app.get('/run-log', (req, res) => {
  logger.info(`${req.method} ${req.path} from ${req.ip} initiated`);
  res.status(200).json({ payload: stateLogs });
  logger.info(`${req.method} ${req.path} from ${req.ip} completed`);
});

// Respond with error for all other requests
app.all('*', (req, res) => {
  logger.warn(`${req.method} ${req.path} from ${req.ip} Not allowed`);
  res.status(400).send('Not allowed');
});

// Creates server
const server = http.createServer(app);

// Server error listener
server.on('error', (err) => {
  logger.error(`Server crashed: ${JSON.stringify(err)}`);
  server.close();
});

// Close server
server.on('close', () => logger.warn('Server closed.'));

// SETUP ORIG MESSAGING
// Connect to rabbitmq
amqp.connect(amqpUrl, (error0, connection) => {
  if (error0) {
    logger.error(`connecting to rabbitmq: ${JSON.stringify(error0)}`);
    server.close();
    throw error0;
  }
  logger.info('connected to rabbitmq');

  // Server starts listening when connection to rabbitmq is succesfull
  server.listen(PORT, () => {
    logger.info(`Started listening on ports: ${PORT}`);
  });

  // Create a channel for transactions in the rabbitmq
  connection.createChannel((error1, channel) => {
    if (error1) {
      logger.error(`connecting to rabbitmq channel: ${JSON.stringify(error1)}`);
      throw error1;
    }
    logger.info('created channel for exchange rabbitmq');
    const exchange = 'devops_amqp_ex';
    const pubTopic = 'my.o';
    const subTopic = 'api';
    // check that the exchange channel exists
    channel.assertExchange(exchange, 'topic', {
      durable: false,
    });

    logger.info(`connect to exchange: '${exchange}'`);

    // Check that queue exists
    channel.assertQueue(
      '',
      {
        exclusive: true,
      },
      (error2, q) => {
        if (error2) {
          logger.error('queue assertion');
          throw error2;
        }
        logger.info('successfully connect to queue');
        // subscribe to the topic
        channel.bindQueue(q.queue, exchange, subTopic);
        logger.info(`subscribed to topic '${subTopic}'`);

        // Consume the messages
        channel.consume(q.queue, (msg) => {
          logger.info(
            `Recieved: ${msg.content.toString()}, topic: ${
              msg.fields.routingKey
            }`
          );
          switch (msg.content.toString()) {
            case states.shutdown:
              if (interval) clearInterval(interval);
              connection.close();
              logger.warn('Shutting down!!!');
              process.exit(0);
              break;
            default:
              logger.warn(
                "I don't understand what to do with message: ",
                msg.content.toString()
              );
              break;
          }
        });
      }
    );

    // assign setState variable to a function
    setState = (nextState) => {
      logger.info(
        `trying to change state from '${currentState}' to '${nextState}'`
      );
      // Keep log of commands
      const date = new Date();
      const dateInISO = date.toISOString();
      const logCMD = `${dateInISO}: ${nextState}`;
      // save command logs
      stateLogs = nextState === states.init ? [logCMD] : [...stateLogs, logCMD];
      // check if next state is same as current state
      if (nextState === currentState) {
        logger.info(`State remains unchanged as ${currentState}`);
        return;
      }

      // Match the next state action
      switch (nextState) {
        case states.init: {
          // sets the state to initial state
          if (interval) clearInterval(interval);
          interval = null;
          messageCounter = 1;
          // update the current state
          logger.info(`state unchanged`);
          currentState = nextState;
          setState(states.running);
          break;
        }
        case states.running: {
          // keeps sending message every 0,5 second.
          interval = setInterval(() => {
            const msg = `MSG_${messageCounter}`;
            channel.publish(exchange, pubTopic, Buffer.from(msg));
            logger.info(`published: ${msg}, topic: ${pubTopic}`);
            messageCounter += 1;
          }, 500);
          // update the current state
          logger.info(`State changed from '${currentState}' to '${nextState}'`);
          currentState = nextState;
          break;
        }
        case states.paused:
          if (interval) clearInterval(interval);
          // update the current state
          logger.info(`State changed from '${currentState}' to '${nextState}'`);
          currentState = nextState;
          break;
        default:
          logger.warn("I don't understand the sent state:", nextState);
      }
    };
    // initialize the state
    logger.info(`state is initialized`);
    setState(states.init);
    setTimeout(() => setState(states.paused), 60000); // to saave memory
  });
});
