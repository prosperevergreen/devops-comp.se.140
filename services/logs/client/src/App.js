import { useState, useRef, useEffect } from 'react';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Alert from 'react-bootstrap/Alert';
import Collapse from 'react-bootstrap/Collapse';
import Button from 'react-bootstrap/Button';
import Spinner from 'react-bootstrap/Spinner';
import axios from 'axios';

const serverURL =
  process.env.NODE_ENV === 'production' ? '' : 'http://localhost:1122';

const services = {
  ORIG: 'orig',
  IMED: 'imed',
  OBSE: 'obse',
  API: 'api',
  HTTPSERV: 'httpserv',
};

const tabsArr = ['orig', 'imed', 'obse', 'api', 'httpserv'];

function App() {
  const [showAlert, setShowAlert] = useState(false);
  const [activeTab, setActiveTab] = useState(services.ORIG);
  const [logData, setLogData] = useState({
    orig: 'Loading...',
    imed: 'Loading...',
    obse: 'Loading...',
    api: 'Loading...',
    httpserv: 'Loading...',
  });
  const timeout = useRef();
  const interval = useRef();
  const showLogs = (tab) => {
    setActiveTab(tab);
  };

  const fetchLogs = async (service) => {
    try {
      const res = await axios.get(`${serverURL}/api/logs/${service}`);
      setLogData((prevLogs) => ({
        ...prevLogs,
        [service]: res.data,
      }));
    } catch (err) {
      setShowAlert(true);
      if (timeout.current) clearTimeout(timeout.current);
      timeout.current = setTimeout(() => setShowAlert(false), 5000);
    }
  };

  useEffect(() => {
    fetchLogs(activeTab);
    if (interval.current) clearInterval(interval.current);
    interval.current = setInterval(() => fetchLogs(activeTab), 5000);
    return () => {
      if (timeout.current) clearTimeout(timeout.current);
      if (interval.current) clearInterval(interval.current);
    };
  }, [activeTab]);

  return (
    <div className="App">
      <Navbar bg="light" variant="light">
        <Container>
          <Navbar.Brand href="#">Services logs</Navbar.Brand>
        </Container>
      </Navbar>
      <Collapse in={showAlert}>
        <div>
          <Alert variant="danger" className="my-0 mx-3">
            Network Error!!!
          </Alert>
        </div>
      </Collapse>
      <Container fluid>
        <Nav
          justify
          variant="tabs"
          defaultActiveKey={activeTab}
          className="bg-light text-danger"
          onSelect={(key) => showLogs(key)}
        >
          {tabsArr.map((tabName, index) => (
            <Nav.Item key={tabName}>
              <Nav.Link eventKey={tabName} as="button">
                {tabName.toUpperCase()}
              </Nav.Link>
            </Nav.Item>
          ))}
        </Nav>
        <Alert variant="info">
          {logData[activeTab]
            .split(/(?:\r\n|\r|\n)/g)
            .filter((data) => data.trim() !== '')
            .map((log, i) => (
              <span key={i}>
                {i + 1}: {log}
                <br />
              </span>
            ))}
        </Alert>
      </Container>
    </div>
  );
}

export default App;
