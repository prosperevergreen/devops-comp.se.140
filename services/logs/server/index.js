const fs = require('fs');
const express = require('express');
const http = require('http');
// states
const states = {
  init: 'INIT',
  running: 'RUNNING',
  paused: 'PAUSED',
  shutdown: 'SHUTDOWN',
};

const PORT = 1122;
const app = express();

// configure server app
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

const services = ['orig', 'imed', 'obse', 'api', 'httpserv'];

// Api for accessing the log file of all the services
services.forEach((service) => {
  app.get(`/api/logs/${service}`, (req, res, next) => {
    try {
      fs.readFile(`/logs/${service}/debug.log`, 'utf8', (err, data) => {
        if (err) throw err;
        res.send(data);
      });
    } catch (err) {
      console.error(`Server crashed: ${err}`);
      next(err);
    }
  });
});

// Handle PUT /state request e.g. SHUTDOWN
app.put('/state', (req, res) => {
  if (req.body.payload === states.shutdown) {
    res.status(200).send('Shutdown');
    process.exit(0);
  }
  res.status(200).send("I don't know what you want");
});

// Creates server
const server = http.createServer(app);

// Server error listener
server.on('error', (err) => {
  console.error(`Server crashed: ${err}`);
  server.close();
});

// Close server
server.on('close', () => console.warn('Server closed.'));

// Server starts listening when connection to rabbitmq is succesfull
server.listen(PORT, () => {
  console.log(`Listening on ports: ${PORT}`);
});
