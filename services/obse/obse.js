const fs = require('fs');
const amqp = require('amqplib/callback_api');
const { createLogger, format, transports } = require('winston');
const http = require('http');

const amqpUrl = process.env.AMQP_URL || 'amqp://0.0.0.0:5672';
const outputFile = '/output/output.txt';
const states = {
  init: 'INIT',
  running: 'RUNNING',
  paused: 'PAUSED',
  shutdown: 'SHUTDOWN',
};
const PORT = 3333;

// Setup logger config
const { printf } = format;
const myFormat = printf(({ level, message, label, timestamp }) => {
  return `[${label}] ${level.toUpperCase()} ${timestamp}: ${message}`;
});
const { combine, timestamp, label } = format;
const logFile = '/logs/debug.log';
const logger = createLogger({
  level: 'debug',
  format: combine(label({ label: 'OBSE' }), timestamp(), myFormat),
  transports: [
    new transports.Console(),
    new transports.File({ filename: logFile }),
  ],
});

// open or create file if it doesn't exist
fs.writeFile(outputFile, '', (err) => {
  if (err) throw err;
  logger.info('Created output.txt file!!!');
});

// Create server
const server = http.createServer((req, res) => {
  res.setHeader('Content-Type', 'text/html');
  if (req.url === '/ping' && req.method === 'GET') {
    res.statusCode = 200;
    return res.end('Pong!!!');
  }
  res.statusCode = 400;
  return res.end('Not allowed!!!');
});

// Server error listener
server.on('error', (err) => {
  logger.error(`Server crashed: ${JSON.stringify(err)}`);
  server.close();
});

// Close server
server.on('close', () => logger.warn('Server closed.'));

// Connect to rabbitmq
amqp.connect(amqpUrl, (error0, connection) => {
  if (error0) {
    logger.error(`connecting to rabbitmq: ${JSON.stringify(error0)}`);
    server.close();
    throw error0;
  }
  logger.info('connected to rabbitmq');
  // Server starts listening when connection to rabbitmq is succesfull
  server.listen(PORT, () => {
    logger.info(`Started listening on ports: ${PORT}`);
  });
  // create channel for transactions
  connection.createChannel((error1, channel) => {
    if (error1) {
      logger.error(`connecting to rabbitmq channel: ${JSON.stringify(error1)}`);
      throw error1;
    }
    logger.info('created to channel for exchange rabbitmq');

    const exchange = 'devops_amqp_ex';
    const subTopic = '#';

    // check that exchange channel exist
    // configure exchange channel
    channel.assertExchange(exchange, 'topic', {
      durable: false,
    });

    logger.info(`connected to exchange: '${exchange}'`);
    // Check that queue exists
    channel.assertQueue(
      '',
      {
        exclusive: true,
      },
      (error2, q) => {
        if (error2) {
          logger.error('queue assertion');
          throw error2;
        }
        logger.info('successfully connect to queue');
        // Subscribe to topics
        channel.bindQueue(q.queue, exchange, subTopic);
        logger.info(`subscribed to topic '${subTopic}'`);

        channel.consume(q.queue, (msg) => {
          logger.info(
            `Recieved: ${msg.content.toString()}, topic: ${
              msg.fields.routingKey
            }`
          );
          switch (msg.content.toString()) {
            // Check if message is shutdown message
            case states.shutdown:
              connection.close();
              logger.warn('Shutting down!!!');
              process.exit(0);
              break;
            default: {
              const date = new Date();
              const dateInISO = date.toISOString();
              const topic = msg.fields.routingKey;
              const message = msg.content.toString();

              // save the sender message and topic info
              const saveData = `${dateInISO} Topic ${topic}: ${message}\n`;

              fs.appendFile(outputFile, saveData, (err) => {
                if (err) {
                  logger.error(`on append file: ${err}`);
                  throw err;
                }
                logger.info(`Saved: ${saveData}`);
              });
            }
          }
        });
      }
    );
  });
});
