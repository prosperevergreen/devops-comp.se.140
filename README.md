# DevOps
## COMP.SE.140 Project – Fall 2021 Project
### Tampere University
### MSc. Software, Web and Cloud

## General understanding

Main learning of this exercise is to have a practical experience with a CD pipeline and teach you
how create such pipeline to automatically build, test and deploy the code to the hosting
environment. In addition, the students will get some basic understanding of the OPS-side. An
average student is assumed to spend about 50h hours with this project.

## Project Instructions
The project instructions can be found at [ProjectDescription-v16.pdf](ProjectDescription-v16.pdf)
## Project Reports and Instructions
The project report and instructions can be found at [EndReport.pdf](EndReport.pdf)
